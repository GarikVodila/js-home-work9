// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent – DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
//     кожен із елементів масиву вивести на сторінку у вигляді пункту списку;


function showElementsList(arr, parentUl = document.body) {

    let ul = document.createElement('ul');
  
    arr.forEach((elem) => {
      let li = document.createElement('li');
      li.append(elem);
      ul.append(li);
    });
    parentUl.prepend(ul);
  }
  
  showElementsList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"] );
  